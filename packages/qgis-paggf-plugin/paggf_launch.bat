@echo off
:: configure the splash screen
set paggf=%~dp0
set osgeo_root=%paggf:apps\paggf=%
call %osgeo_root%\bin\o4w_env.bat
call %osgeo_root%\bin\py3_env.bat
copy /Y "%OSGEO4W_ROOT%\apps\paggf\custom_ui.conf" "%TEMP%" > nul
echo splashpath=%OSGEO4W_ROOT:\=\\%\\apps\\qgis-ltr\\python\\plugins\\PAGGF\\img\\>>"%TEMP%\custom_ui.conf"

:: make sure the db is started
python -m pglite start

:: launch QGIS
start "QGIS" /B "%OSGEO4W_ROOT%\bin\qgis-ltr-bin.exe" --customizationfile "%TEMP%\custom_ui.conf" --profile paggf %1

