@echo off
:: create the db cluster
::

:: 0. checks if the old service exists
sc query paggf_db | find "STATE" >nul
if errorlevel 1 goto :noservice
:: 1. make sure the service is stopped
"%OSGEO4W_ROOT%\bin\nircmd" elevate net stop paggf_db
:waitforstop
echo Waiting for service to stop ...
timeout /t 1 /nobreak >nul
sc query paggf_db | find "STATE" | find "STOPPED" >nul
if errorlevel 1 goto :waitforstop
:: 2. unregister the old service if present
"%OSGEO4W_ROOT%\bin\nircmd" elevate "%OSGEO4W_ROOT%\bin\pg_ctl" unregister -N paggf_db
:noservice

:: 3. delete the old data directory
rmdir /s /q "%OSGEO4W_ROOT%\apps\paggf\.paggf_db"
:: 4. init the cluster
call %OSGEO4W_ROOT%\bin\py3_env.bat
python -m pglite reset
python -m pglite init
python -m pglite start

:: create the db
python -m pglite create paggf
:: create extension and grant user
echo create extension paggf cascade | pglite psql paggf
echo drop user if exists paggf_op | pglite psql paggf
echo create user paggf_op | pglite psql paggf
echo grant paggf_operator to paggf_op | pglite psql paggf

:: populate the db with some existing parcels

::ogrinfo %OSGEO4W_ROOT%\apps\paggf\inventaire_total.gpkg | findstr /r "^[0-9]" > l.txt
::for /f "tokens=2 delims= " %%l in (l.txt) do (
::  echo %%l
::  ogr2ogr -progress -append -f "PostgreSQL" PG:"dbname=paggf port=55432" -nln import.import_idc_parcel_from_polygon %OSGEO4W_ROOT%\apps\paggf\inventaire_total.gpkg %%l  
::)

:: make sure there is no qgis running
nircmd killprocess qgis-ltr-bin.exe

:: overwrite QGIS3.ini
mkdir "%APPDATA%\QGIS\QGIS3\profiles\paggf\QGIS"
copy /Y "%OSGEO4W_ROOT%\apps\paggf\QGIS3.ini" "%APPDATA%\QGIS\QGIS3\profiles\paggf\QGIS"
mkdir "%APPDATA%\postgresql"
copy /Y "%OSGEO4W_ROOT%\apps\paggf\pg_service.conf" "%APPDATA%\postgresql\.pg_service.conf"

:: install shortcuts on desktop
nircmd shortcut "%OSGEO4W_ROOT%\apps\paggf\paggf_launch.bat" "~$folder.desktop$" "SIF-TD Inventaire" "%OSGEO4W_ROOT%\apps\paggf\paggf_encodage_cid.qgs" "%OSGEO4W_ROOT%\apps\paggf\icon.ico" "" "min"
nircmd shortcut "%OSGEO4W_ROOT%\apps\paggf\paggf_launch.bat" "~$folder.desktop$" "SIF-TD Immatriculation" "%OSGEO4W_ROOT%\apps\paggf\paggf_encodage_bm.qgs" "%OSGEO4W_ROOT%\apps\paggf\icon.ico" "" "min"
nircmd shortcut "%OSGEO4W_ROOT%\apps\paggf\paggf_launch.bat" "~$folder.desktop$" "SIF-TD Enregistrement" "%OSGEO4W_ROOT%\apps\paggf\paggf_encodage_ef.qgs" "%OSGEO4W_ROOT%\apps\paggf\icon.ico" "" "min"
nircmd shortcut "%OSGEO4W_ROOT%\apps\paggf\paggf_launch.bat" "~$folder.desktop$" "Administration" "%OSGEO4W_ROOT%\apps\paggf\paggf_admin.qgs" "%OSGEO4W_ROOT%\apps\paggf\icon.ico" "" "min"
