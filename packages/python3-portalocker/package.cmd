::--------- Package settings --------
:: package name
set P=python3-portalocker
:: version
set V=1.6.0
:: package version
set B=3

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat

pip install portalocker==%V%

tar -C %OSGEO4W_ROOT% -cvjf %PKG_BIN% apps/Python37/Lib/site-packages/portalocker

::--------- Installation
scp %PKG_BIN% %R%
cd %HERE%
scp setup.hint %R%
