--- "postgis\\gserialized_spgist_nd.c.orig"
+++ "postgis\\gserialized_spgist_nd.c"
@@ -264,14 +264,16 @@ containND(CubeGIDX *cube_box, GIDX *query)
 
 PG_FUNCTION_INFO_V1(gserialized_spgist_config_nd);
 
-PGDLLEXPORT Datum gserialized_spgist_config_nd(PG_FUNCTION_ARGS)
+Datum gserialized_spgist_config_nd(PG_FUNCTION_ARGS)
 {
 	spgConfigOut *cfg = (spgConfigOut *)PG_GETARG_POINTER(1);
 	Oid boxoid = postgis_oid_fcinfo(fcinfo, GIDXOID);
 
 	cfg->prefixType = boxoid;
 	cfg->labelType = VOIDOID; /* We don't need node labels. */
+	#if POSTGIS_PGSQL_VERSION >= 110
 	cfg->leafType = boxoid;
+	#endif
 	cfg->canReturnData = false;
 	cfg->longValuesOK = false;
 
@@ -284,7 +286,7 @@ PGDLLEXPORT Datum gserialized_spgist_config_nd(PG_FUNCTION_ARGS)
 
 PG_FUNCTION_INFO_V1(gserialized_spgist_choose_nd);
 
-PGDLLEXPORT Datum gserialized_spgist_choose_nd(PG_FUNCTION_ARGS)
+Datum gserialized_spgist_choose_nd(PG_FUNCTION_ARGS)
 {
 	spgChooseIn *in = (spgChooseIn *)PG_GETARG_POINTER(0);
 	spgChooseOut *out = (spgChooseOut *)PG_GETARG_POINTER(1);
@@ -308,7 +310,7 @@ PGDLLEXPORT Datum gserialized_spgist_choose_nd(PG_FUNCTION_ARGS)
  */
 PG_FUNCTION_INFO_V1(gserialized_spgist_picksplit_nd);
 
-PGDLLEXPORT Datum gserialized_spgist_picksplit_nd(PG_FUNCTION_ARGS)
+Datum gserialized_spgist_picksplit_nd(PG_FUNCTION_ARGS)
 {
 	spgPickSplitIn *in = (spgPickSplitIn *)PG_GETARG_POINTER(0);
 	spgPickSplitOut *out = (spgPickSplitOut *)PG_GETARG_POINTER(1);
@@ -391,7 +393,7 @@ PGDLLEXPORT Datum gserialized_spgist_picksplit_nd(PG_FUNCTION_ARGS)
  */
 PG_FUNCTION_INFO_V1(gserialized_spgist_inner_consistent_nd);
 
-PGDLLEXPORT Datum gserialized_spgist_inner_consistent_nd(PG_FUNCTION_ARGS)
+Datum gserialized_spgist_inner_consistent_nd(PG_FUNCTION_ARGS)
 {
 	spgInnerConsistentIn *in = (spgInnerConsistentIn *)PG_GETARG_POINTER(0);
 	spgInnerConsistentOut *out = (spgInnerConsistentOut *)PG_GETARG_POINTER(1);
@@ -523,7 +525,7 @@ PGDLLEXPORT Datum gserialized_spgist_inner_consistent_nd(PG_FUNCTION_ARGS)
  */
 PG_FUNCTION_INFO_V1(gserialized_spgist_leaf_consistent_nd);
 
-PGDLLEXPORT Datum gserialized_spgist_leaf_consistent_nd(PG_FUNCTION_ARGS)
+Datum gserialized_spgist_leaf_consistent_nd(PG_FUNCTION_ARGS)
 {
 	spgLeafConsistentIn *in = (spgLeafConsistentIn *)PG_GETARG_POINTER(0);
 	spgLeafConsistentOut *out = (spgLeafConsistentOut *)PG_GETARG_POINTER(1);
@@ -592,7 +594,7 @@ PGDLLEXPORT Datum gserialized_spgist_leaf_consistent_nd(PG_FUNCTION_ARGS)
 
 PG_FUNCTION_INFO_V1(gserialized_spgist_compress_nd);
 
-PGDLLEXPORT Datum gserialized_spgist_compress_nd(PG_FUNCTION_ARGS)
+Datum gserialized_spgist_compress_nd(PG_FUNCTION_ARGS)
 {
 	char gidxmem[GIDX_MAX_SIZE];
 	GIDX *result = (GIDX *)gidxmem;