--- "liblwgeom\\lwout_wkt.c.orig"	2019-02-20 13:58:41.329965600 +0100
+++ "liblwgeom\\lwout_wkt.c"	2019-02-20 14:19:09.178862800 +0100
@@ -29,6 +29,7 @@
 
 static void lwgeom_to_wkt_sb(const LWGEOM *geom, stringbuffer_t *sb, int precision, uint8_t variant);
 
+#define buffer_size 128
 
 /*
 * ISO format uses both Z and M qualifiers.
@@ -85,7 +86,7 @@ static void ptarray_to_wkt_sb(const POINTARRAY *ptarray, stringbuffer_t *sb, int
 	/* OGC only includes X/Y */
 	uint32_t dimensions = 2;
 	uint32_t i, j;
-	static size_t buffer_size = 128;
+	//static size_t buffer_size = 128;
 	char coord[buffer_size];
 
 	/* ISO and extended formats include all dimensions */
