::--------- Package settings --------
:: package name
set P=tempus-core
:: version
set V=3.1.0
:: package version
set B=1
:: build dependencies
set BUILD_DEPS=boost-devel-vc14

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1

if "%1"=="test" (
wget --progress=bar:force https://gitlab.com/Oslandia/tempus_core/repository/master/archive.tar.bz2 -O tempus.tar.bz2 || goto :error
) else (
wget --progress=bar:force https://gitlab.com/Oslandia/tempus_core/repository/archive.tar.bz2?ref=v%V% -O tempus.tar.bz2 || goto :error
)

tar xjf tempus.tar.bz2
cd tempus_core*

mkdir build
mkdir install
cd build

cmake -G "NMake Makefiles" ^
      -DCMAKE_BUILD_TYPE=RelWithDebInfo ^
      -DCMAKE_INSTALL_PREFIX=%HERE%\install ^
      "-DPOSTGRESQL_PG_CONFIG=C:\osgeo4w64\bin\pg_config.exe" ^
      -DBOOST_LIBRARYDIR=C:\osgeo4w64\lib ^
      -DBOOST_INCLUDEDIR=C:\osgeo4w64\include\boost-1_63 ^
      -DBoost_COMPILER=-vc140 ^
      -DBUILD_DOC=OFF ^
      -DBUILD_TOOLS=OFF ^
      ..  || goto :error

nmake  || goto :error
nmake install  || goto :error

cd %HERE%/install
mkdir bin
copy lib\tempus.dll bin || goto :error
cd %HERE%

:: binary archive
tar --transform 's,install,apps/tempus,' -C install -cvjf %PKG_BIN% bin/tempus.dll lib/tempus.lib || goto :error

:: source archive
tar -C %HERE% --transform 's,^,osgeo4w/,' -cvjf %PKG_SRC% package.cmd setup.hint || goto :error

::--------- Installation
scp %PKG_BIN% %PKG_SRC% %R% || goto :error
cd %HERE%
scp setup.hint %R% || goto :error
goto :EOF

:error
echo Build failed
exit /b 1
  
  
